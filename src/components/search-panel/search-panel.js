import React, { Component } from 'react';
import './search-panel.css';

export default class SearchPanel extends Component{

    state = {
        query: ''
    }

    onQueryChange = (e) => {
        const query = e.target.value;
        this.setState({query});
        this.props.onQueryChange(query);
    }
    

    render(){
        const searchText = "Type here to search";
        return(
            <input type="text" 
                    className="form-control" 
                    placeholder={searchText}                  
                    onChange={(e) => this.onQueryChange(e)}
                    value = {this.state.query} />
        )
    }
}
