import React, { Component } from 'react';
import './add-item-form.css';

export default class AddItemForm extends Component {

  state = {
    label:''
  }

  onLabelChange = (e) => {
    this.setState({
      label: e.target.value
    })  
  }
  
  onSubmit = (e) => {
    e.preventDefault();
    const label = this.state.label;
    if(label.trim().length > 0){
      this.props.addItem(label);
      this.setState({
        label: ''
      })
    }
  }
  

  render() {
    return (
      <form className="add-item-form d-flex"
            onSubmit={ this.onSubmit }>
        <input 
          className="form-control new-item-title"
          placeholder="What needs to be done"
          onChange= { this.onLabelChange }
          value={ this.state.label }></input>
        <button 
          type="submit"
          className="btn btn-success">Add Item</button>
      </form>
    );
  }
}
