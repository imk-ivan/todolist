import React from 'react';
import { shallow } from 'enzyme';
import AddItemForm from './add-item-form';

describe('AddItemForm', () => {
  test('matches snapshot', () => {
    const wrapper = shallow(<AddItemForm />);
    expect(wrapper).toMatchSnapshot();
  });
});
