import React, { Component } from 'react'
import TodoList from '../todo-list';
import AppHeader from '../app-header';
import SearchPanel from '../search-panel';
import ItemStatusFilter from '../item-status-filter';
import AddItemForm from '../add-item-form';
import './app.css';

export default class App extends Component {

  constructor(){
    super();

    this.maxId = 100;

    this.state = {
      todoData : [
        this.createTodoItem('Drink cofee'),
        this.createTodoItem('Learn React'),
        this.createTodoItem('Build awesome project'),
      ],
      query: '',
      filter: 'all'
    }
  }

  deleteItem = (id) => {
    console.log(id);
    this.setState(({ todoData }) => {
        return {
          todoData: todoData.filter((item) => item.id !== id)
        }
      });
      
  }
  
  addNewItem = (title) => { 
    const newItem = this.createTodoItem(title);
    this.setState(({ todoData })=>{
      return {
        todoData: [...todoData, newItem]
      }
    })
  }
  
  createTodoItem = (label) => {
    return {
      label: label,
      important: false,
      done: false,
      id: this.maxId++
    };
  }
  
  onToggleImportant = (id) => {
    this.setState(({todoData}) => {
      return {
        todoData: this.toggleProperty(todoData, id, 'important')
      }
    })
  }
  
  onToggleDone = (id) => {
    this.setState(({todoData}) => {
      return {
        todoData: this.toggleProperty(todoData, id, 'done')
      }
    })
  }

  toggleProperty = (arr, id, propName) => {

    const updatedData = arr.map((item) => {
      if(item.id === id){
        item[propName] = !item[propName];
      }
      return item;
    });
    
    return updatedData;
  }

  onQueryChange = (query) => {
    this.setState({
      query: query
    })
  }

  onFilterChange = (filter) => {
    this.setState({
      filter: filter
    }) 
  }
  
  
  search = (arr, query) => {
    let result = arr;
    if(query.length > 0){
      result = arr.filter(item => item.label.toLowerCase().includes(query.toLowerCase()));
    }
    return result;
  }
  
  filterItems = (arr, filter) => {
    let result;
    switch(filter){
      case 'all':
        result = arr;
        break;
      case 'active':
        result = arr.filter(item => !item.done);
        break;
      case 'done':
        result = arr.filter(item => item.done);
        break;
      default:
        result = arr;
    }
    return result;
  }
  

  render() {
    const { todoData, query, filter } = this.state;

    const filteredData = this.filterItems(this.search(todoData, query), filter);

    const todoItemsCount = filteredData.filter(item => !item.done).length;
    const doneItemsCount = filteredData.length - todoItemsCount;
    return (
      <div className="container todo-app">
        <div className="row">
          <div className="col-6 main-content">
            <AppHeader toDo={todoItemsCount} done={doneItemsCount}/>
            <div className="top-panel">
              <div className="row">
                <div className="col-lg-7 search-panel-column">
                  <SearchPanel onQueryChange={ this.onQueryChange }/>
                </div>
                <div className="col-lg-5 filter-column justify-content-lg-end">
                  <ItemStatusFilter filter={ filter }
                                    onFilterChange = { this.onFilterChange }/>
                </div>
              </div>     
            </div>
           
            <TodoList items={ filteredData }
                      onDeleted={ this.deleteItem }
                      onToggleDone={ this.onToggleDone }
                      onToggleImportant={ this.onToggleImportant }></TodoList>
            <AddItemForm
              addItem={(title)=>this.addNewItem(title)}/>
          </div>
        </div>
      </div>
    )
  }
}