import React, { Component } from 'react'
import './todo-list-item.css';

export default class TodoListItem extends Component {

    render() {
        const { label,
                done,
                important, 
                onDeleted, 
                onToggleImportant, 
                onToggleDone
             } = this.props;

        let labelClass = "todo-item";
        if(done)
        {
            labelClass += ' done';
        }
        if(important)
        {
            labelClass += ' important';
        }

        return (
            <div className={labelClass}>
                <span className="item-label noselect"
                      onClick={onToggleDone}>
                          {label}
                </span>
                <div className="item-actions">
                    <button type="button" 
                            className="btn btn-outline-danger remove-item"
                            onClick={onDeleted}>
                        <i className="fa fa-trash" aria-hidden="true"></i>
                    </button>
                    <button type="button" 
                            className="btn btn-outline-success check-item"
                            onClick={onToggleImportant}>
                        <i className="fa fa-exclamation" aria-hidden="true"></i>
                    </button>
                </div> 
            </div>
        )
    }
}