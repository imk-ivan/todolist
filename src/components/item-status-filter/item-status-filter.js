import React, { Component } from 'react'
import './item-status-filter.css';

export default class ItemStatusFilter extends Component {

    buttonsData = [
        {value:'all', lable: 'All'},
        {value:'active', lable: 'Active'},
        {value:'done', lable: 'Done'}
    ]

    render() {
        const { filter, onFilterChange } = this.props;

        const buttons = this.buttonsData.map((button) => {
            const buttonClass = filter === button.value ? 'btn-info': 'btn-outline-info';
            return(
                <button type="button"
                        className={`btn ${buttonClass}`}
                        key={button.value}
                        onClick={() => onFilterChange(button.value) }>{button.lable}</button>
            )
        })

        return (
            <div className="btn-group" role="group" aria-label="Task filter">
                {buttons}
            </div>
        )
    }
}