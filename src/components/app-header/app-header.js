import React from 'react';
import './app-header.css';

const AppHeader = ({ toDo, done }) =>{
    return (
        <div className="app-header">
            <h1>ToDo List</h1>
            <div className="status-info">{toDo} more to do, {done} done</div>
        </div>
        
    )
}

export default AppHeader;
